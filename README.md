# project-specification

- resume

learning electric engineering at a university<br>
was something what i rarely used in later work life...<br>
early i got jobs at electric or mechanic institutes<br>
about teaching and coding..<br>

later many years i worked instrumentation and electric in heavy style industry<br>
- - glas/TV tube production and 
- - engineering for construction and startup polyester & nylon plants worldwide

but again with major in the software part of it, <br>
like design, buy, configure, teach, startup & tune process control systems...<br>
while the work ( and the money ) was good, the places i worked and lived at<br>
were the horror... no wonder all my former colleagues are divorced and alcoholic.<br>

- biorock work

when the bank account was full i quit for early retirement<br>
and on the airplane to Koh Samui Thailand i read a magazine article<br>
about Prof. Wolf Hartmut Hilbertz and BIOROCK<br>

and meet and joined TPS as BIOROCK volunteer
- - first projects on samui beach steelwork.. 
- - lab models electrolysis
- - cable and powersupply / solarpanel installation
- - measurements and data collection and EXCEL back-calc tool
- - more tools over the years: project calculation, dome / tunnel design, first biorock electric models 
- - underwater power supply / BOLPS calc tools
- - biorock model and teaching material

but 15 years later, i am old,<br>
after all that extra unmaintained excel file/tools i will try here a summary tool<br>
to pass on my little knowhow as a <br>
- new online tool

what can be used via browser<br>
with a 3D show ( acc. the features shown at project p5-template, play with it interactive https://editor.p5js.org/kll/present/XGvq2Yuxl or find its outprint here: p5-template.pdf)<br>
parameter input or show ( like spreadsheet )<br>
- - tunnel design input and steel calc and current setpoint output
- - site ( distances ) and cable type input and cable calc output
- - biorock model and power requirements with cable losses and power converter losses
- - PS working point

lots of description/documentation text ( what i never did in my EXCEL tools ), that also WORD people can understand it.<br>

BOM lists for steel and cable<br>

add:<br>
TPS request: cost calculation support like for steel / cable / GRID power consumption<br>
TPS request: optional BOLPS version<br>

so future generations of BIOROCKers can use, copy <br>
and improve it ( yes sorry, its in javascript now, not EXCEL ).<br>

author: KLL<br>
date: 7/2020<br>
www: http://kll.engineering-news.org<br>
lic: ( CC-BY-SA 4.0 )<br>

